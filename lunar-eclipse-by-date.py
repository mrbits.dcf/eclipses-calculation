#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
lunar-eclipse-by-date.py

A simple python script to calculate the separation of
the Moon from the center of Earth's umbra. When the
separation is the lowest, then it is the peak of the eclipse.

This is for the lunar eclipse of a specific date

Use ./lunar-eclipse-by-date.py start-date-with-hour end-date-with-hour

$ ./lunar-eclipse-by-date.py 2018-01-31-10-00-00 2018-01-31-17-00-00

This will calculate for January, 31 2018 Lunar eclipse.

If there is no eclipse, high Moon elevations are expected
'''

import ephem
import sys
from datetime import datetime, timedelta

startdate = sys.argv[1]
enddate = sys.argv[2]

curtime = datetime.strptime(startdate, '%Y-%m-%d-%H-%M-%S')
endtime = datetime.strptime(enddate, '%Y-%m-%d-%H-%M-%S')
resdelta = timedelta(seconds=60)

moon = ephem.Moon()
sun = ephem.Sun()
observer = ephem.Observer()
observer.elevation = -6371000  # place observer in the center of the Earth
observer.pressure = 0          # disable refraction
observer.lat = '-23.5505'
observer.long = '-46.6333'

while curtime <= endtime:
    observer.date = curtime.strftime('%Y/%m/%d %H:%M:%S')

    # computer the position of the sun and the moon with respect to the observer
    moon.compute(observer)
    sun.compute(observer)

    # calculate separation between the moon and the sun, convert
    # it from radians to degrees, substract it by 180°
    sun_moon_distance = abs((float(ephem.separation(moon, sun)) / 0.01745329252))
    sep = abs((float(ephem.separation(moon, sun)) / 0.01745329252) - 180)

    print "{0}^{1}^{2}".format(curtime.strftime('%Y/%m/%d^%H:%M:%S'), sep, sun_moon_distance)

    # advance by time interval defined in resdelta
    curtime += resdelta
