#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
lunar-eclipse-prediction.py

Shows the occurences of a lunar eclipse in the 21st century.
Works by iterating every hour in the 21st century and calculating if the
separation between the Moon and the Sun is less than 1.5° from 180°.
The number 1.5° is hardcoded for simplicity, and can forecast penumbral
eclipses. For more accuracy, it should be computed from the distance of the Moon and the Sun.
'''

import ephem
from tqdm import tqdm
from datetime import datetime, timedelta

curtime = datetime(2021, 11, 18, 0, 0, 0)        # start time
endtime = datetime(2039, 12, 31, 23, 59, 59)   # end time

moon = ephem.Moon()
sun = ephem.Sun()
observer = ephem.Observer()
observer.elevation = -6371000    # place observer in the center of the Earth
observer.pressure = 0            # disable refraction

runs = 870987

pbar = tqdm(total=runs + 1)

while curtime <= endtime:
    observer.date = curtime.strftime('%Y/%m/%d %H:%M:%S')

    # computer the position of the sun and the moon with respect to the observer
    moon.compute(observer)
    sun.compute(observer)

    # calculate separation between the moon and the sun, convert
    # it from radians to degrees, substract it by 180°
    sep = abs((float(ephem.separation(moon, sun)) / 0.01745329252) - 180)

    # Eclipse happens if Sun-Earth-Moon alignment is less than 0.9°.
    # this should detect all total and partial eclipses, but is
    # hit-and-miss for penumbral eclipses.
    if sep < 1.59754941:
        print(curtime.strftime('%Y/%m/%d %H:%M:%S'), sep)
        # an eclipse cannot happen more than once in a day,
        # so we skip 24 hours when an eclipse is found
        curtime += timedelta(days=1)
    else:
        # advance an hour if eclipse is not found
        curtime += timedelta(hours=1)
    #pbar.update(1)
    

pbar.close()
