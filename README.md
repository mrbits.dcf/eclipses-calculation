Eclipse Calculation Scripts
===========================

[![pipeline status](https://gitlab.com/mrbits.dcf/eclipses-calculation/badges/master/pipeline.svg)](https://gitlab.com/mrbits.dcf/eclipses-calculation/commits/master)

Some very simple Python scripts to demonstrate it is possible to predict the occurrences of eclipse without using the Saros cycle. This is intended for the victims of the flat-Earth dogma, or anyone susceptible to it.

These scripts use [pyephem](https://github.com/brandon-rhodes/pyephem) library.

Usage
-----

* **Clone this repository**

```
git clone https://gitlab.com/mrbits.dcf/eclipses-calculation.git
```

* **Python installed globally**

```
sudo pip install -r requirements.txt
```

Execute scripts with ./script.py parameters

* **Python in a virtualenv**

After activate your virtualenv

```
pip install -r requirements.txt
```

Execute scripts with ./script.py parameters

* **Using Docker**

Build the image

```
docker build -t eclipses-calculation .
```

Execute with

```
docker run --rm -it eclipses-calculation /app/script.py parameters
```

* **For the lazy ones**

Forget everything above and just pull docker image from registry.

```
docker pull mrbits/eclipses-calculation:latest
```

Execute with

```
docker run --rm -it mrbits/eclipses-calculation:latest /app/script.py parameters
```

